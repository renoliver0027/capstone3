import React from "react";

const UseContext = React.createContext();

export const UseProvider = UseContext.Provider;

export default UseContext;
