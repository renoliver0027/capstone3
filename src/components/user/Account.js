import { useContext, useEffect, useState } from "react";
import UseContext from "../../UseContext";
import { Form } from "react-bootstrap";
import EditAccount from "./EditAccount";

export default function Account({ onUpdate }) {
  const { user } = useContext(UseContext);

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  //   const [password, setPassword] = useState(user.password);

  useEffect(() => {
    setName(user.name);
    setEmail(user.email);
    //   setPassword(user.password);
  }, [user]);

  const handleAccountUpdate = (updatedDetails) => {
    setName(updatedDetails.name);
    setEmail(updatedDetails.email);

    onUpdate(updatedDetails);
  };

  return (
    <>
      <Form className="w-50 mx-auto">
        <h1 className="my-5">Account</h1>

        <Form.Group controlId="email">
          <Form.Label>Name</Form.Label>
          <Form.Control type="text" value={name} placeholder={name} readOnly />
        </Form.Group>

        <Form.Group controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="text"
            value={email}
            placeholder={email}
            readOnly
          />
        </Form.Group>

        {/* <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password}
            placeholder={password}
            readOnly
          />
        </Form.Group> */}

        <EditAccount onUpdate={handleAccountUpdate} />
      </Form>
    </>
  );
}
