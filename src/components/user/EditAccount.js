import { useContext } from "react";
import { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import UseContext from "../../UseContext";

export default function EditAccount({ onUpdate }) {
  const { user } = useContext(UseContext);

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  //   const [currentPassword, setCurrentPassword] = useState("");
  //   const [newPassword, setNewPassword] = useState("");
  //   const [confirmPassword, setConfirmPassword] = useState("");
  //   const [isActive, setIsActive] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);

  const handleShowModal = () => setShowEditModal(true);
  const closeEditModal = () => setShowEditModal(false);

  const editAccount = async (event) => {
    event.preventDefault();

    // if (currentPassword !== user.password) {
    //   Swal.fire({
    //     title: "Invalid Current Password",
    //     text: "Please enter the correct current password.",
    //     icon: "error",
    //   });
    //   return;
    // }

    let requestBody = {
      name: user.name,
      email: user.email,
    };

    if (name !== "") {
      requestBody.name = name;
    }

    if (email !== "") {
      requestBody.email = email;
    }

    // if (newPassword !== "") {
    //   requestBody.password = newPassword;
    // }

    fetch(
      `${process.env.REACT_APP_API_URL}/api/users/${localStorage.getItem(
        "userId"
      )}`,
      {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(requestBody),
      }
    )
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setName(result.name);
          setEmail(result.email);

          onUpdate({
            name: result.name,
            email: result.email,
          });

          Swal.fire({
            title: "Account Updated Successfully",
            text: result.message,
            icon: "success",
          });

          closeEditModal();
        } else {
          Swal.fire({
            title: "Update Failed",
            text: result.message,
            icon: "error",
          });
        }
      });
  };

  //   useEffect(() => {
  //     if (newPassword !== confirmPassword) {
  //       setIsActive(false);
  //     } else {
  //       setIsActive(true);
  //     }
  //   }, [newPassword, confirmPassword]);

  return (
    <>
      <Button className="mt-3" variant="success" onClick={handleShowModal}>
        Edit Account
      </Button>

      {/* Edit Modal */}
      <Modal show={showEditModal} onHide={closeEditModal}>
        <Form onSubmit={(event) => editAccount(event)}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Account</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="userName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                value={name}
                placeholder={user.name}
                onChange={(event) => setName(event.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="userEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                value={email}
                placeholder={user.email}
                onChange={(event) => setEmail(event.target.value)}
              />
            </Form.Group>

            {/* <Form.Group controlId="userCurrentPassword">
              <Form.Label>Current Password</Form.Label>
              <Form.Control
                type="password"
                value={currentPassword}
                onChange={(event) => setCurrentPassword(event.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="userNewPassword">
              <Form.Label>New Password</Form.Label>
              <Form.Control
                type="password"
                value={newPassword}
                onChange={(event) => setNewPassword(event.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="userConfirmPassword">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type="password"
                value={confirmPassword}
                onChange={(event) => setConfirmPassword(event.target.value)}
              />
            </Form.Group> */}
          </Modal.Body>

          <Modal.Footer>
            <Button
              className="me-auto"
              variant="secondary"
              onClick={closeEditModal}
            >
              Close
            </Button>

            <Button
              variant="success"
              type="submit"
              // disabled={!isActive}
            >
              Update
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
