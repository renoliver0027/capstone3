import { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Checkout({ cartProducts, totalSubtotal }) {
  const [showModal, setShowModal] = useState(false);

  const handleClose = () => setShowModal(false);
  const handleShow = () => setShowModal(true);

  const handleCheckout = () => {
    fetch(`${process.env.REACT_APP_API_URL}/api/orders/checkout`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        cartId: localStorage.getItem("cartId"),
        userId: localStorage.getItem("userId"),
        products: cartProducts,
        totalAmount: totalSubtotal,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          Swal.fire({
            title: "Checkout Successful",
            text: result.message,
            icon: "success",
          });

          handleClose();

          localStorage.removeItem("cartId");
        } else {
          Swal.fire({
            title: "Something went wrong",
            text: result.message,
            icon: "error",
          });
        }
      });
  };

  return (
    <>
      {cartProducts.length === 0 ? null : (
        <Button variant="warning" onClick={handleShow}>
          Proceed to Checkout
        </Button>
      )}

      <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Proceed to Checkout?</Modal.Title>
        </Modal.Header>

        <Modal.Footer>
          <Button className="me-auto" variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="danger" onClick={() => handleCheckout()}>
            Proceed
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
