import { useState } from "react";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function AddToCart({ productId, productPrice }) {
  const [quantity, setQuantity] = useState(0);

  const handleAdd = () => {
    setQuantity(quantity + 1);
  };

  const handleSubtract = () => {
    if (quantity > 0) {
      setQuantity(quantity - 1);
    }
  };

  const addToCart = (productId, productPrice) => {
    if (localStorage.getItem("cartId") === null) {
      fetch(`${process.env.REACT_APP_API_URL}/api/cart`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          userId: localStorage.getItem("userId"),
          productId,
          productPrice,
          quantity,
        }),
      })
        .then((response) => response.json())
        .then((result) => {
          if (result) {
            console.log(result);

            localStorage.setItem("cartId", result._id);

            setQuantity(0);

            Swal.fire({
              title: "Added to Cart Successfully",
              text: result.message,
              icon: "success",
            });
          } else {
            Swal.fire({
              title: "Something went wrong",
              text: result.message,
              icon: "error",
            });
          }
        });
    } else {
      fetch(
        `${process.env.REACT_APP_API_URL}/api/cart/${localStorage.getItem(
          "cartId"
        )}/add`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify({
            userId: localStorage.getItem("userId"),
            productId,
            productPrice,
            quantity,
          }),
        }
      )
        .then((response) => response.json())
        .then((result) => {
          if (result) {
            console.log(result);

            setQuantity(0);

            Swal.fire({
              title: "Added to Cart Successfully",
              text: result.message,
              icon: "success",
            });
          } else {
            Swal.fire({
              title: "Something went wrong",
              text: result.message,
              icon: "error",
            });
          }
        });
    }
  };

  return (
    <>
      <div className="d-flex align-items-center justify-content-center">
        <Button variant="outline-secondary" onClick={handleSubtract}>
          -
        </Button>

        <div
          className="form-control mx-2"
          style={{
            width: "50px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {quantity}
        </div>

        <Button variant="outline-secondary" onClick={handleAdd}>
          +
        </Button>
      </div>

      <Button
        className="mt-3"
        variant="success"
        style={{ width: "100%" }}
        onClick={() => addToCart(productId, productPrice)}
      >
        <i className="fi fi-rr-shopping-cart"></i>
        <span className="ms-2">Add to Cart</span>
      </Button>
    </>
  );
}
