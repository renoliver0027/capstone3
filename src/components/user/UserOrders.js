import { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function UserOrders() {
  const [orders, setOrders] = useState([]);
  const [orderData, setOrderData] = useState([]);
  const [totalSubtotal, setTotalSubtotal] = useState(0);
  const [purchasedOn, setPurchasedOn] = useState("");

  useEffect(() => {
    const fetchOrders = () => {
      fetch(
        `${process.env.REACT_APP_API_URL}/api/orders/${localStorage.getItem(
          "userId"
        )}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      )
        .then((response) => response.json())
        .then((result) => {
          if (result) {
            setOrders(result.products);

            setPurchasedOn(result.purchasedOn);
          } else {
            setOrders(null);
          }
        });
    };

    fetchOrders();
  }, []);

  useEffect(() => {
    const fetchProductData = async () => {
      const productDataPromises = orders.map(async (product) => {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/api/products/${product.productId}`,
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        );
        const result = await response.json();

        return {
          ...result,
          quantity: product.quantity,
          subtotal: product.subtotal,
          purchasedOn: new Date(purchasedOn).toLocaleString(),
        };
      });

      const fetchedProductData = await Promise.all(productDataPromises);
      setOrderData(fetchedProductData);

      const total = fetchedProductData.reduce(
        (acc, product) => acc + product.subtotal,
        0
      );
      setTotalSubtotal(total);
    };

    fetchProductData();
  }, [orders, purchasedOn]);

  return (
    <>
      {orders.length === 0 ? (
        <>
          <div style={{ height: "5rem", width: "100%" }}>
            <h1>No orders yet</h1>
          </div>

          <Button as={Link} to="/products" variant="success">
            Shop Now
          </Button>
        </>
      ) : (
        <>
          <Table striped bordered hover className="mt-3">
            <thead>
              <tr>
                <th>Item ID</th>
                <th>Image</th>
                <th>Name</th>
                <th>Price Php</th>
                <th>Quantity</th>
                <th>Subtotal</th>
                <th>Purchased On</th>
              </tr>
            </thead>

            <tbody className="align-center">
              {orderData.map((item, index) => (
                <>
                  <tr key={item._id}>
                    <td>{item._id}</td>
                    <td>
                      <img
                        src={item.image}
                        alt={item.name}
                        style={{ width: "5rem", height: "5rem" }}
                      />
                    </td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>{item.quantity}</td>
                    <td>{item.subtotal}</td>
                    <td>{item.purchasedOn}</td>
                  </tr>
                </>
              ))}
            </tbody>
          </Table>

          <div style={{ display: "flex", justifyContent: "end" }}>
            <div className="me-5">
              <p>
                <strong>Total:</strong>
              </p>
            </div>

            <div className="ms-4">
              <p>₱ {totalSubtotal}</p>
            </div>
          </div>
        </>
      )}
    </>
  );
}
