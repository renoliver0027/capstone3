import { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import EditCart from "./EditCart";
import Checkout from "./Checkout";

export default function CartItems({ cartProducts }) {
  const [cartData, setCartData] = useState([]);
  const [totalSubtotal, setTotalSubtotal] = useState(0);

  useEffect(() => {
    const fetchProductData = async () => {
      const productDataPromises = cartProducts.map(async (product) => {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/api/products/${product.productId}`,
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        );
        const result = await response.json();

        return {
          ...result,
          quantity: product.quantity,
          subtotal: product.subtotal,
        };
      });

      const fetchedProductData = await Promise.all(productDataPromises);
      setCartData(fetchedProductData);

      const total = fetchedProductData.reduce(
        (acc, product) => acc + product.subtotal,
        0
      );
      setTotalSubtotal(total);
    };

    fetchProductData();
  }, [cartProducts]);

  const handleQuantityUpdate = (index, newQuantity, newSubtotal) => {
    const updatedCartData = [...cartData];
    updatedCartData[index].quantity = newQuantity;
    updatedCartData[index].subtotal = newSubtotal;
    setCartData(updatedCartData);

    const newTotal = updatedCartData.reduce(
      (acc, product) => acc + product.subtotal,
      0
    );
    setTotalSubtotal(newTotal);
  };

  return (
    <>
      {cartProducts.length === 0 ? (
        <div style={{ height: "5rem", width: "100%" }}>
          <h1>Your cart is empty</h1>
        </div>
      ) : (
        <>
          <Table striped bordered hover className="mt-3">
            <thead>
              <tr>
                <th>Product ID</th>
                <th>Image</th>
                <th>Name</th>
                <th>Price Php</th>
                <th colSpan={2}>Quantity</th>
                <th>Subtotal</th>
              </tr>
            </thead>

            <tbody className="align-center">
              {cartData.map((product, index) => (
                <>
                  <tr key={product._id}>
                    <td>{product._id}</td>
                    <td>
                      <img
                        src={product.image}
                        alt={product.name}
                        style={{ width: "5rem", height: "5rem" }}
                      />
                    </td>
                    <td>{product.name}</td>
                    <td>{product.price}</td>
                    <td>{product.quantity}</td>

                    <td>
                      <EditCart
                        productId={product._id}
                        productPrice={product.price}
                        initialQuantity={product.quantity}
                        onUpdate={(newQuantity, newSubtotal) =>
                          handleQuantityUpdate(index, newQuantity, newSubtotal)
                        }
                      />
                    </td>

                    <td>{product.subtotal}</td>
                  </tr>
                </>
              ))}
            </tbody>
          </Table>

          <div style={{ display: "flex", justifyContent: "end" }}>
            <div className="me-5">
              <p>
                <strong>Total:</strong>
              </p>
            </div>

            <div className="ms-4">
              <p>₱ {totalSubtotal}</p>
            </div>
          </div>
        </>
      )}

      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Button as={Link} to="/products" variant="success">
          {cartProducts.length === 0 ? "Shop Now" : "Continue Shopping"}
        </Button>

        <Checkout cartProducts={cartProducts} totalSubtotal={totalSubtotal} />
      </div>
    </>
  );
}
