import { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

const EditCart = ({ productId, productPrice, initialQuantity, onUpdate }) => {
  const [quantity, setQuantity] = useState(initialQuantity);
  const [showModal, setShowModal] = useState(false);

  const handleClose = () => setShowModal(false);
  const handleShow = () => setShowModal(true);

  const handleQuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value, 10);
    setQuantity(newQuantity);
  };

  const handleUpdateClick = () => {
    fetch(
      `${process.env.REACT_APP_API_URL}/api/cart/${localStorage.getItem(
        "cartId"
      )}/edit`,
      {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          productId,
          productPrice,
          quantity,
        }),
      }
    )
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          Swal.fire({
            title: "Cart Updated",
            text: result.message,
            icon: "success",
          });

          const newSubtotal = productPrice * quantity;

          onUpdate(quantity, newSubtotal);

          handleClose();
        } else {
          Swal.fire({
            title: "Something went wrong",
            text: result.message,
            icon: "error",
          });

          handleClose();
        }
      });
  };

  return (
    <>
      <Button variant="link" onClick={handleShow}>
        Edit
      </Button>

      <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Quantity</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form.Group>
            <Form.Label>Quantity</Form.Label>
            <Form.Control
              type="number"
              value={quantity}
              onChange={handleQuantityChange}
            />
          </Form.Group>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => handleUpdateClick()}>
            Update
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default EditCart;
