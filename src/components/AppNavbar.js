import { useContext } from "react";
import { Navbar, Container, Nav, Button } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import UseContext from "../UseContext";

export default function AppNavbar() {
  const { user } = useContext(UseContext);

  return (
    <>
      <Navbar data-bs-theme="light">
        <Container fluid>
          <Navbar.Brand as={Link} to="/" className="nav__brand">
            <img src="/pineapple.svg" alt="pineapple" width={35} height={35} />{" "}
            <span className="nav__brand-title">Fruitasan</span>
          </Navbar.Brand>

          <Nav className="ms-auto nav__links">
            <Nav.Link as={NavLink} to="/">
              <span className="NavLinks">Home</span>
            </Nav.Link>

            <Nav.Link as={NavLink} to="/products">
              <span className="NavLinks">Products</span>
            </Nav.Link>

            {user.id !== null ? (
              user.isAdmin ? (
                <>
                  <Nav.Link as={NavLink} to="/dashboard">
                    <span className="NavLinks">Dashboard</span>
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/logout">
                    <span className="NavLinks">Log Out</span>
                  </Nav.Link>
                </>
              ) : (
                <>
                  <Nav.Link as={NavLink} to="/profile">
                    <span className="NavLinks">Profile</span>
                  </Nav.Link>

                  <Nav.Link as={NavLink} to="/logout">
                    <span className="NavLinks">Log Out</span>
                  </Nav.Link>
                </>
              )
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login">
                  <span className="NavLinks">Log In</span>
                </Nav.Link>

                <Nav.Link as={NavLink} to="/register">
                  <Button className="NavLinks" variant="outline-success">
                    Sign Up
                  </Button>
                  {/* <span className="NavLinks">Sign Up</span> */}
                </Nav.Link>
              </>
            )}
          </Nav>
        </Container>
      </Navbar>
    </>
  );
}
