import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function AdminUser({ user_id, fetchUsers, isAdmin }) {
  const userToAdmin = (userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/${userId}/toAdmin`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          Swal.fire({
            title: `${result.name} is now an Admin`,
            text: result.message,
            icon: "success",
          });

          fetchUsers();
        } else {
          Swal.fire({
            title: "Failed :(",
            text: result.message,
            icon: "error",
          });

          fetchUsers();
        }
      });
  };

  const adminToUser = (userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/${userId}/toUser`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          Swal.fire({
            title: `${result.name} is now a Guest User`,
            text: result.message,
            icon: "success",
          });

          fetchUsers();
        } else {
          Swal.fire({
            title: "Failed :(",
            text: result.message,
            icon: "error",
          });

          fetchUsers();
        }
      });
  };

  return (
    <>
      {isAdmin ? (
        <Button
          variant="warning"
          size="sm"
          onClick={() => adminToUser(user_id)}
          style={{ width: "50%" }}
        >
          Guest
        </Button>
      ) : (
        <Button
          variant="success"
          size="sm"
          onClick={() => userToAdmin(user_id)}
          style={{ width: "50%" }}
        >
          Admin
        </Button>
      )}
    </>
  );
}
