import { useContext, useEffect, useState } from "react";
import { Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import UseContext from "../../UseContext";
import AddToCart from "../user/AddToCart";

export default function ProductCard({ product }) {
  const { user } = useContext(UseContext);

  const [productId, setProductId] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState(null);

  useEffect(() => {
    if (product) {
      setProductId(product._id);
      setName(product.name);
      setDescription(product.description);
      setPrice(product.price);
      setImage(product.image);
    } else {
      setProductId("");
      setName("");
      setDescription("");
      setPrice(0);
      setImage(null);
    }
  }, [product]);

  return (
    <>
      <Card className="my-3">
        <Card.Img
          variant="top"
          src={image}
          style={{
            height: "200px",
            width: "100%",
            objectFit: "cover",
          }}
        />
        <Card.Body>
          <Card.Title>{name}</Card.Title>

          <Card.Text>
            {description.length > 50
              ? description.substring(0, 50) + "..."
              : description}
          </Card.Text>

          <Card.Subtitle>Price: </Card.Subtitle>
          <Card.Text>₱ {price}</Card.Text>

          {user.id !== null ? (
            user.isAdmin ? (
              <Button
                as={Link}
                to="/dashboard"
                variant="outline-success"
                style={{ width: "100%" }}
              >
                Edit Product
              </Button>
            ) : (
              <AddToCart productId={productId} productPrice={price} />
            )
          ) : (
            <Button
              as={Link}
              to="/login"
              variant="success"
              style={{ width: "100%" }}
            >
              Log In to Shop
            </Button>
          )}
        </Card.Body>
      </Card>
    </>
  );
}
