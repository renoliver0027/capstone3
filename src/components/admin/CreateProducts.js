import { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function CreateProducts() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState(null);

  const navigate = useNavigate();

  const handleImageUpload = (event) => {
    const file = event.target.files[0];

    setImage(file);

    TransformFile(file);
  };

  const TransformFile = (image) => {
    const reader = new FileReader();

    if (image) {
      reader.readAsDataURL(image);
      reader.onloadend = () => {
        setImage(reader.result);
      };
    } else {
      setImage(null);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    await fetch(`${process.env.REACT_APP_API_URL}/api/products/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name,
        description,
        price,
        image,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setName("");
          setDescription("");
          setPrice("");
          setImage(null);

          Swal.fire({
            title: "Successfully added a product",
            text: result.message,
            icon: "success",
          });

          navigate("/dashboard");
        } else {
          Swal.fire({
            title: "Something went wrong",
            text: result.message,
            icon: "error",
          });
        }
      });
  };

  return (
    <Form className="w-50 mx-auto" onSubmit={handleSubmit}>
      <Form.Group controlId="fileUpload">
        <Form.Label>Upload a File</Form.Label>
        <Form.Control
          type="file"
          accept="image/*"
          onChange={handleImageUpload}
        />
      </Form.Group>

      <Form.Group controlId="productName">
        <Form.Label>Title</Form.Label>
        <Form.Control
          type="text"
          value={name}
          onChange={(event) => setName(event.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="productDescription">
        <Form.Label>Description</Form.Label>
        <Form.Control
          type="text"
          value={description}
          onChange={(event) => setDescription(event.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="productPrice">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="text"
          value={price}
          onChange={(event) => setPrice(event.target.value)}
        />
      </Form.Group>
      <Button className="mt-3" type="submit">
        Add
      </Button>
    </Form>
  );
}
