import React, { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function AllOrders() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/orders/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => setOrders(result))
      .catch((error) => console.error("Error fetching orders", error));
  }, []);

  return (
    <>
      {orders.map((order) => (
        <div key={order._id}>
          <h3>Order ID: {order._id}</h3>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Basket ID</th>
                <th>Quantity</th>
                <th>Subtotal</th>
              </tr>
            </thead>
            <tbody>
              {order.products.map((product) => (
                <tr key={product._id}>
                  <td>{product.productId}</td>
                  <td>{product.quantity}</td>
                  <td>{product.subtotal}</td>
                </tr>
              ))}
            </tbody>
            <tfoot>
              <tr>
                <th>Total Amount</th>
                <td colSpan="2">{order.totalAmount}</td>
              </tr>

              <tr>
                <th>Purchased On</th>
                <td colSpan="2">
                  {new Date(order.purchasedOn).toLocaleString()}
                </td>
              </tr>
            </tfoot>
          </Table>
        </div>
      ))}
      <Button variant="success" as={Link} to="/dashboard">
        Go Back to dashboard
      </Button>
    </>
  );
}
