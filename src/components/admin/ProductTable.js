import { Table } from "react-bootstrap";
import ArchiveProduct from "./ArchiveProduct";

export default function ProductTable({ products, fetchProducts }) {
  return (
    <Table striped bordered hover className="mt-3">
      <thead>
        <tr>
          <th>Product #</th>
          <th>Image</th>
          <th>Name</th>
          <th>Description</th>
          <th>Price Php</th>
          <th colSpan={2}>isActive</th>
        </tr>
      </thead>

      <tbody className="align-center">
        {products.map((product, index) => (
          <>
            <tr key={product._id}>
              <td>{index + 1}</td>
              <td>
                <img
                  src={product.image}
                  alt={product.name}
                  style={{ width: "5rem", height: "5rem" }}
                />
              </td>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>{product.price}</td>
              <td>{product.isActive ? "True" : "False"}</td>
              <td>
                <ArchiveProduct
                  product_id={product._id}
                  isActive={product.isActive}
                  fetchProducts={fetchProducts}
                />
              </td>
            </tr>
          </>
        ))}
      </tbody>
    </Table>
  );
}
