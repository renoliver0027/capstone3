import { Button } from "react-bootstrap";
import Swal from "sweetalert2";
import EditProduct from "./EditProduct";

export default function ArchiveProduct({
  product_id,
  fetchProducts,
  isActive,
}) {
  const archiveProduct = (productId) => {
    fetch(
      `${process.env.REACT_APP_API_URL}/api/products/${productId}/archive`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          Swal.fire({
            title: "Product Archived",
            text: result.message,
            icon: "success",
          });

          fetchProducts();
        } else {
          Swal.fire({
            title: "Archive Failed :(",
            text: result.message,
            icon: "error",
          });

          fetchProducts();
        }
      });
  };

  const activateProduct = (productId) => {
    fetch(
      `${process.env.REACT_APP_API_URL}/api/products/${productId}/activate`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          Swal.fire({
            title: "Product Activated",
            text: result.message,
            icon: "success",
          });

          fetchProducts();
        } else {
          Swal.fire({
            title: "Activation Failed :(",
            text: result.message,
            icon: "error",
          });

          fetchProducts();
        }
      });
  };

  return (
    <>
      {isActive ? (
        <Button
          variant="warning"
          size="sm"
          onClick={() => archiveProduct(product_id)}
          style={{ width: "100%" }}
        >
          Archive
        </Button>
      ) : (
        <Button
          variant="primary"
          size="sm"
          onClick={() => activateProduct(product_id)}
          style={{ width: "100%" }}
        >
          Activate
        </Button>
      )}
      <br />

      <EditProduct
        product_id={product_id}
        fetchProducts={fetchProducts}
        isActive={isActive}
      />
    </>
  );
}
