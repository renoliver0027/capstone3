import { Table } from "react-bootstrap";
import AdminUser from "./AdminUser";

export default function UserTable({ users, fetchUsers }) {
  return (
    <Table striped bordered hover className="mt-3">
      <thead>
        <tr>
          <th>User ID</th>
          <th>Name</th>
          <th>Email</th>
          <th>Role</th>
          <th>Change role to: </th>
        </tr>
      </thead>

      <tbody className="align-center">
        {users.map((user, index) => (
          <>
            <tr key={user._id}>
              <td>{user._id}</td>
              <td>{user.name}</td>
              <td>{user.email}</td>
              <td>{user.isAdmin ? "Admin" : "Guest"}</td>
              <td>
                <AdminUser
                  user_id={user._id}
                  isAdmin={user.isAdmin}
                  fetchUsers={fetchUsers}
                />
              </td>
            </tr>
          </>
        ))}
      </tbody>
    </Table>
  );
}
