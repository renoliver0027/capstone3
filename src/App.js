import { useState } from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { UseProvider } from "./UseContext";
import AppNavbar from "./components/AppNavbar";
import AllOrders from "./components/admin/AllOrders";
import CreateProducts from "./components/admin/CreateProducts";
import AdminDashboard from "./pages/AdminDashboard";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Products from "./pages/Products";
import Profile from "./pages/Profile";
import Register from "./pages/Register";

function App() {
  const [user, setUser] = useState({
    id: null,
    name: null,
    email: null,
    password: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  return (
    <>
      <UseProvider value={{ user, setUser, unsetUser }}>
        <BrowserRouter>
          <Container fluid>
            <AppNavbar />

            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/profile" element={<Profile />} />
              <Route path="/products" element={<Products />} />
              <Route path="/create" element={<CreateProducts />} />
              <Route path="/dashboard" element={<AdminDashboard />} />
              <Route path="/allOrders" element={<AllOrders />} />
            </Routes>
          </Container>
        </BrowserRouter>
      </UseProvider>
    </>
  );
}

export default App;
