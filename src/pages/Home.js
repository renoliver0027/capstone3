import { Link } from "react-router-dom";

export default function Home() {
  return (
    <section className="home__section">
      {/* Left pane */}
      <div md={6} sm={12} className="home__left">
        <div className="home__title">
          <div className="home__title-box1">
            <h1>
              <span className="home__title-left1">Organic fruit products </span>
              <span className="home__title-left2">
                brought right to your door.
              </span>
            </h1>
          </div>

          <div className="home__title-box2">
            <span className="home__para">
              We believe that fresh natural organic fruit products should be
              accessible to all.
            </span>
          </div>

          <div className="home__title-box3">
            <Link className="home__btn" to="/products">
              <span className="home__btn-title">Start Shopping</span>
            </Link>
          </div>
        </div>
      </div>

      {/* Right pane */}
      <div className="home__right">
        <div className="home__img-box1">
          <img src="/strawberry.png" alt="fruits" className="home__img1"></img>
        </div>

        <div className="home__img-box-2-3">
          <div className="home__img-box2">
            <img src="/orange.png" alt="fruits" className="home__img2"></img>
          </div>

          <div className="home__img-box3">
            <img src="/melon.png" alt="fruits" className="home__img3"></img>
          </div>
        </div>
      </div>
    </section>
  );
}
