import { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import ProductCard from "../components/admin/ProductCard";

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/products/active`)
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setProducts(result);
        }
      });
  }, []);

  return (
    <>
      <div className="text-center">
        <h1>Products</h1>
      </div>

      <Container>
        <Row>
          {products.map((product) => (
            <Col key={product._id} xs={12} sm={6} md={4} lg={3}>
              <ProductCard product={product} />
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
}
