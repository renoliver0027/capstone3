import { useContext, useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UseContext from "../UseContext";

export default function Login() {
  const { setUser } = useContext(UseContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  const navigate = useNavigate();

  function loginUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          localStorage.setItem("token", result.accessToken);
          localStorage.setItem("userId", result.userId);

          setEmail("");
          setPassword("");

          retrieveUserDetails(result.accessToken, result.userId);

          Swal.fire({
            title: "Login Successful",
            text: result.message,
            icon: "success",
          });

          if (result.isAdmin === true) {
            navigate("/dashboard");
          } else {
            navigate("/profile");
          }
        } else {
          Swal.fire({
            title: "Something went wrong",
            text: result.message,
            icon: "error",
          });
        }
      });
  }

  const retrieveUserDetails = (token, userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/user`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: userId,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setUser({
            id: result._id,
            name: result.name,
            email: result.email,
            password: result.password,
            isAdmin: result.isAdmin,
          });
        } else {
          setUser({
            id: null,
            name: null,
            email: null,
            password: null,
            isAdmin: null,
          });
        }
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <>
      <Form className="w-50 mx-auto" onSubmit={(event) => loginUser(event)}>
        <h1 className="my-5">Log In</h1>

        <Form.Group controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="text"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            required
          />
        </Form.Group>

        <div className="mt-2">
          <p>
            No account yet? <Link to="/register">Register</Link>
          </p>
        </div>

        <Button variant="success" type="submit" disabled={!isActive}>
          Log In
        </Button>
      </Form>
    </>
  );
}
