import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function Register() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  const navigate = useNavigate();

  function registerUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/api/users/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        email,
        password,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setName("");
          setEmail("");
          setPassword("");
          setConfirmPassword("");

          Swal.fire({
            title: "Register Successful",
            text: result.message,
            icon: "success",
          });

          navigate("/login");
        } else {
          Swal.fire({
            title: "Something went wrong",
            text: result.message,
            icon: "error",
          });
        }
      });
  }

  useEffect(() => {
    if (
      name !== "" &&
      email !== "" &&
      password !== "" &&
      confirmPassword !== "" &&
      password === confirmPassword
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, email, password, confirmPassword]);

  return (
    <>
      <Form className="w-50 mx-auto" onSubmit={(event) => registerUser(event)}>
        <h1 className="my-5">Register</h1>

        <Form.Group controlId="name">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            value={name}
            onChange={(event) => setName(event.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="text"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="confirmPassword">
          <Form.Label>Confirm Password</Form.Label>
          <Form.Control
            type="password"
            value={confirmPassword}
            onChange={(event) => setConfirmPassword(event.target.value)}
            required
          />
        </Form.Group>

        <div className="mt-2">
          <p>
            Already have an account? <Link to="/login">Log In</Link>
          </p>
        </div>

        <Button variant="success" type="submit" disabled={!isActive}>
          Register
        </Button>
      </Form>
    </>
  );
}
