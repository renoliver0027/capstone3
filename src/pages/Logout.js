import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UseContext from "../UseContext";

export default function Logout() {
  const { unsetUser, setUser } = useContext(UseContext);

  unsetUser();

  useEffect(() => {
    setUser({
      id: null,
      isAdmin: null,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <Navigate to="/login" />;
}
