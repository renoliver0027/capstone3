import { useEffect, useState } from "react";
import { Button, Tab, Tabs } from "react-bootstrap";
import { Link } from "react-router-dom";
import ProductTable from "../components/admin/ProductTable";
import UserTable from "../components/admin/UserTable";

export default function AdminDashboard() {
  const [products, setProducts] = useState([]);
  const [users, setUsers] = useState([]);

  const fetchProducts = () =>
    fetch(`${process.env.REACT_APP_API_URL}/api/products`)
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setProducts(result);
        }
      });

  const fetchUsers = () =>
    fetch(`${process.env.REACT_APP_API_URL}/api/users`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setUsers(result);
        }
      });

  useEffect(() => {
    fetchProducts();
    fetchUsers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Tabs
        defaultActiveKey="products"
        transition={false}
        id="noanim-tab-example"
        className="mb-3"
      >
        <Tab eventKey="products" title="Products">
          <Button as={Link} to="/create">
            Add Product
          </Button>

          <ProductTable products={products} fetchProducts={fetchProducts} />
        </Tab>

        <Tab eventKey="users" title="Users">
          <Button as={Link} to="/allOrders">
            All Orders
          </Button>

          <UserTable users={users} fetchUsers={fetchUsers} />
        </Tab>
      </Tabs>
    </>
  );
}
