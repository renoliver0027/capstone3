import { useContext, useEffect, useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import UseContext from "../UseContext";
import CartItems from "../components/user/CartItems";
import UserOrders from "../components/user/UserOrders";
import Account from "../components/user/Account";

export default function Profile() {
  const { user } = useContext(UseContext);

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [cartProducts, setCartProducts] = useState([]);

  const retrieveCartDetails = (userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/api/cart/${userId}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          localStorage.setItem("cartId", result._id);

          setCartProducts(result.products);
        } else {
          setCartProducts(null);
        }
      });
  };

  useEffect(() => {
    if (user.id !== null) {
      setName(user.name);
      setEmail(user.email);

      retrieveCartDetails(user.id);
    } else {
      setName("");
      setEmail("");
    }
  }, [user]);

  const handleAccountUpdate = (updatedDetails) => {
    setName(updatedDetails.name);
    setEmail(updatedDetails.email);
  };

  return (
    <>
      <div>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <h1>Your Shopping Cart ({cartProducts.length})</h1>
        </div>

        <div
          style={{ display: "flex", justifyContent: "center" }}
          className="mt-3"
        >
          <p className="me-5">{name}</p>
          <p className="me-5">|</p>
          <p className="me-5">{email}</p>
        </div>
      </div>

      <Tabs
        defaultActiveKey="cart"
        transition={false}
        id="noanim-tab-example"
        className="mb-3"
      >
        <Tab eventKey="cart" title="Your Cart">
          <CartItems cartProducts={cartProducts} />
        </Tab>

        <Tab eventKey="orders" title="Order History">
          <UserOrders />
        </Tab>

        <Tab eventKey="account" title="Account">
          <Account onUpdate={handleAccountUpdate} />
        </Tab>
      </Tabs>
    </>
  );
}
